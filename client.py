import socket

def main():
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_socket.connect(("localhost", 12345))

    number = 0

    while number < 100:
        data = client_socket.recv(1024).decode()
        number = int(data) + 1
        print("Sending to server:", number)
        client_socket.send(str(number).encode())

    client_socket.close()

if __name__ == "__main__":
    main()