import socket

def main():
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind(("localhost", 12345))
    server_socket.listen()

    print("Waiting for connection...")
    connection, address = server_socket.accept()
    print("Connected to:", address)

    number = 1

    while number <= 100:
        print("Sending to client:", number)
        connection.send(str(number).encode())
        data = connection.recv(1024).decode()
        number = int(data) + 1


    connection.close()
    server_socket.close()

if __name__ == "__main__":
    main()